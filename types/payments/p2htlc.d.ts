import { Payment, PaymentOpts } from './index';
export declare function p2htlc(inPayment: Payment, opts?: PaymentOpts): Payment;
