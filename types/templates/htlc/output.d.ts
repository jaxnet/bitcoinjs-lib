import { Stack } from '../../payments';
export declare function check(script: Buffer | Stack): boolean;
export declare namespace check {
    var toJSON: () => string;
}
export declare function getSubType(script: Buffer | Stack): string;
