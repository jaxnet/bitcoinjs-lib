export declare type OpCode = number;
export declare const OPS: {
    [index: string]: number;
};
export declare const REVERSE_OPS: {
    [index: number]: string;
};
