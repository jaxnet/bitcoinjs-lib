import { ECPairInterface, Psbt, Transaction } from './index';
export interface SignerInfo {
    pubkey: Buffer;
    input: number;
}
export interface PsbtHelper {
    tx: Transaction;
    redeemScript: Buffer;
    parentTx: Buffer;
    psbt?: Psbt;
    signers?: SignerInfo[];
}
export declare function addSignatureToTx(opts: PsbtHelper, inputToSign: number, keypair: ECPairInterface): PsbtHelper;
export declare function validateRaw2SigCSTX(cstx: Transaction, sourceShardFundsLockingTXHash: string, destinationShardFundsLockingTXHash: string, eadPubKey: Buffer, eadAddress: string, receiverAddress: string, sourceShardAmount: number, destinationShardAmount: number, sourceShardMultiSigRedeemScript: Buffer, destinationShardMultiSigRedeemScript: Buffer, network: any): [number | null, // Source shard amount input index.
// Source shard amount input index.
number | null, // Source shard fee input index.
// Source shard fee input index.
number | null, // Destination shard amount input index.
// Destination shard amount input index.
number | null, // Destination shard fee input index.
// Destination shard fee input index.
number | null, // Source shard output index.
// Source shard output index.
number | null, // Destination shard output index.
// Destination shard output index.
Error | null];
