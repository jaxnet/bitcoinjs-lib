'use strict';
Object.defineProperty(exports, '__esModule', { value: true });
const index_1 = require('./index');
// tslint:disable-next-line:no-shadowed-variable
function addSignatureToTx(opts, inputToSign, keypair) {
  const psbt = new index_1.Psbt();
  psbt.setVersion(opts.tx.version);
  for (const item of opts.tx.outs) {
    psbt.addOutput({ script: item.script, value: item.value });
  }
  const signers = [];
  // tslint:disable-next-line:no-shadowed-variable
  for (const input of opts.tx.ins) {
    psbt.addInput({
      hash: input.hash,
      index: input.index,
      sequence: input.sequence,
    });
  }
  const input = opts.tx.ins[inputToSign];
  const multisigLockScript = index_1.payments.p2mstl({
    output: opts.redeemScript,
  });
  // tslint:disable-next-line:typedef
  const extractSignatures = () => {
    // tslint:disable-next-line:no-shadowed-variable
    const partialSig = [];
    if (typeof input.script === 'undefined' || input.script.length === 0) {
      return partialSig;
    }
    const chunks = index_1.script.decompile(input.script);
    for (const sig of chunks) {
      if (typeof sig === 'number') {
        continue;
      }
      for (const pubkey of multisigLockScript.pubkeys) {
        const compressed = pubkey.length < 60;
        const kp = index_1.ECPair.fromPublicKey(pubkey, { compressed });
        try {
          const sigData = index_1.script.signature.decode(sig);
          const hashForSignature = opts.tx.hashForSignature(
            inputToSign,
            opts.redeemScript,
            sigData.hashType,
          );
          const isSigned = kp.verify(hashForSignature, sigData.signature);
          if (isSigned) {
            partialSig.push({
              pubkey: kp.publicKey,
              signature: sig,
            });
            signers.push({
              pubkey: kp.publicKey,
              input: inputToSign,
            });
            break;
          }
        } catch (e) {
          // continue
        }
      }
      return partialSig;
    }
  };
  const partialSig = extractSignatures();
  psbt.updateInput(inputToSign, {
    partialSig: partialSig,
    redeemScript: opts.redeemScript,
    nonWitnessUtxo: opts.parentTx,
  });
  psbt.signInput(inputToSign, keypair);
  psbt.finalizeInput(inputToSign, true);
  const newTx = psbt.extractTransaction(true, true);
  for (let i = 0; i < opts.tx.ins.length; i++) {
    if (i === inputToSign) continue;
    newTx.ins[i].script = opts.tx.ins[i].script;
  }
  return {
    parentTx: opts.parentTx,
    tx: newTx,
    redeemScript: opts.redeemScript,
    signers: signers,
  };
}
exports.addSignatureToTx = addSignatureToTx;
const CSTXVersion = 4;
// CSTX always moves funds between 2 shards only.s
const ShardsCount = 2;
// CSTX is always going to be spent from multi. sig address (funds lock address),
// so it always has only one input per shard.
const RequiredInputsCountPerShard = 2;
const RequiredTotalInputsCount = RequiredInputsCountPerShard * ShardsCount;
// CSTX always moves funds to only one destination address in one shard,
// so it always has only one output per shard.
const RequiredOutputsCountPerShard = 2;
const RequiredTotalOutputsCount = RequiredOutputsCountPerShard * ShardsCount;
function validateRaw2SigCSTX(
  cstx,
  sourceShardFundsLockingTXHash,
  destinationShardFundsLockingTXHash,
  eadPubKey,
  eadAddress,
  receiverAddress,
  sourceShardAmount,
  destinationShardAmount,
  sourceShardMultiSigRedeemScript,
  destinationShardMultiSigRedeemScript,
  network,
) {
  const sourceShardFundsLockingRawTXHashBinary = Buffer.from(
    sourceShardFundsLockingTXHash,
    'hex',
  );
  const destinationShardFundsLockingRawTXHashBinary = Buffer.from(
    destinationShardFundsLockingTXHash,
    'hex',
  );
  function validationFailed(msg) {
    return [
      null,
      null,
      null,
      null,
      null,
      null,
      Error('CSTX validation failed: ' + msg),
    ];
  }
  // Check: TX must have special type (CSTX).
  if (cstx.version !== CSTXVersion) {
    return validationFailed(
      'version of the CSTX does not correspond to the required one. ' +
        `Required ${CSTXVersion}, present ${cstx.version}`,
    );
  }
  // Check: amount of inputs correspond to CSTX configuration.
  if (cstx.ins.length !== RequiredTotalInputsCount) {
    return validationFailed(
      'amount of inputs does not correspond to the required amount ' +
        `(${RequiredTotalInputsCount})`,
    );
  }
  // Check: amount of outputs correspond to CSTX configuration.
  if (cstx.outs.length !== RequiredTotalOutputsCount) {
    return validationFailed(
      'amount of outputs does not correspond to the required amount ' +
        `(${RequiredTotalOutputsCount})`,
    );
  }
  const cstxInputsAndOutputs = getInputsAndOutputsOfShards(
    cstx,
    sourceShardFundsLockingRawTXHashBinary,
    destinationShardFundsLockingRawTXHashBinary,
  );
  const err = cstxInputsAndOutputs[4];
  if (err != null) {
    return validationFailed(
      `could not determine CSTX inputs/outputs configuration: ${err}`,
    );
  }
  const sourceShardAmountInputIndex = cstxInputsAndOutputs[0];
  const destinationShardAmountInputIndex = cstxInputsAndOutputs[1];
  const sourceShardOutputIndex = cstxInputsAndOutputs[2];
  const destinationShardOutputIndex = cstxInputsAndOutputs[3];
  // Check: inputs and outputs are non nil.
  if (sourceShardAmountInputIndex == null) {
    return validationFailed('invalid CSTX input (source shard input)');
  }
  if (destinationShardAmountInputIndex == null) {
    return validationFailed('invalid CSTX input (destination shard input)');
  }
  if (sourceShardOutputIndex == null) {
    return validationFailed('invalid CSTX output (source shard output)');
  }
  if (destinationShardOutputIndex == null) {
    return validationFailed('invalid CSTX output (destination shard)');
  }
  //
  // Source shard checks
  //
  const sourceShardOutput = cstx.outs[sourceShardOutputIndex];
  try {
    // Check: source output points to only one address.
    const sourceShardDestinationAddress = index_1.address.fromOutputScript(
      sourceShardOutput.script,
      network,
    );
    // console.log('source shard destination address:', sourceShardDestinationAddress.toString())
    // Check: source output points to the EAD.
    if (sourceShardDestinationAddress.toString() !== eadAddress) {
      return validationFailed(
        'source shard output does not point to the EAD: ' +
          `required address is: ${eadAddress},` +
          `output points to: ${sourceShardDestinationAddress}`,
      );
    }
  } catch (e) {
    return validationFailed('source shard output could not be parsed');
  }
  // Check: source output uses exactly the same amount of tokens,
  // as it has been declared during exchange initialisation.
  if (sourceShardOutput.value !== sourceShardAmount) {
    return validationFailed(
      `source shard output uses invalid amount: required = ${sourceShardAmount}` +
        `, actually used = ${sourceShardOutput.value}`,
    );
  }
  //
  // Destination shard checks
  //
  const destinationShardOutput = cstx.outs[destinationShardOutputIndex];
  try {
    // Check: destination output points to only one address.
    const destinationShardDestinationAddress = index_1.address.fromOutputScript(
      destinationShardOutput.script,
      network,
    );
    // console.log('destination shard destination address:', destinationShardDestinationAddress.toString())
    // Check: destination output points to the EAD.
    if (destinationShardDestinationAddress.toString() !== receiverAddress) {
      return validationFailed(
        'destination shard output does not point to the receiver: ' +
          `required address is: ${receiverAddress},` +
          `output points to: ${destinationShardDestinationAddress}`,
      );
    }
  } catch (e) {
    return validationFailed(
      'CSTX validation failed: source shard output could not be parsed',
    );
  }
  // Check: source output uses exactly the same amount of tokens,
  // as it has been declared during exchange initialisation.
  if (destinationShardOutput.value !== destinationShardAmount) {
    return validationFailed(
      `destination shard output uses invalid amount: required = ${destinationShardAmount}` +
        `, actually used = ${destinationShardOutput.value}`,
    );
  }
  // Check: TX must be signed by the EAD.
  const validationErr = validate2SigEADPartialSignatures(
    cstx,
    eadPubKey,
    sourceShardMultiSigRedeemScript,
    destinationShardMultiSigRedeemScript,
  );
  const sourceShardFeeIndex = (sourceShardAmountInputIndex + 1) % 2;
  const destinationShardFeeIndex =
    ((destinationShardAmountInputIndex -
    2 + // смещение в левую группу (0,1) (2,3)
      1) %
      2) +
    2; // смещение обратно в правую группу.
  return [
    sourceShardAmountInputIndex,
    sourceShardFeeIndex,
    destinationShardAmountInputIndex,
    destinationShardFeeIndex,
    sourceShardOutputIndex,
    destinationShardOutputIndex,
    validationErr,
  ];
}
exports.validateRaw2SigCSTX = validateRaw2SigCSTX;
// CSTX does not specifies what inputs/outputs belongs to which shard.
// The only rule present is the next: the first input and first output belongs to the same shard,
// as well as seconds input and the second output belongs to the second shard.
// But there is no any metadata in the CSTX that allows to determine
// what input belongs to which shard in terms of shard ID.
//
// getInputsAndOutputsOfShards tries to find input, that corresponds to the sourceShardFundsLockingTxHash.
// In case if this input is present - all other outputs and the rest input could be identified and returned.
function getInputsAndOutputsOfShards(
  cstx,
  sourceShardFundsLockingTxHash,
  destinationShardFundsLockingTxHash,
) {
  const nonInitializedShardIndex = -1;
  let sourceShardIndex = nonInitializedShardIndex;
  let destinationShardIndex = nonInitializedShardIndex;
  let i = 0;
  for (const input of cstx.ins) {
    if (input.hash.compare(sourceShardFundsLockingTxHash)) {
      sourceShardIndex = i;
      break;
    }
    i++;
  }
  if (sourceShardIndex === nonInitializedShardIndex) {
    const err = Error(
      'no corresponding input found that points to the original funds lock TX input (source shard)',
    );
    return [null, null, null, null, err];
  }
  destinationShardIndex = (sourceShardIndex + 2) % 4;
  const destinationShardLockTxInput = cstx.ins[destinationShardIndex];
  if (
    !destinationShardLockTxInput.hash.compare(
      destinationShardFundsLockingTxHash,
    )
  ) {
    const err = Error(
      "corresponding input in destination shard doesn't match lock TX",
    );
    return [null, null, null, null, err];
  }
  return [
    sourceShardIndex,
    destinationShardIndex,
    sourceShardIndex,
    destinationShardIndex,
    null,
  ];
}
function validate2SigEADPartialSignatures(
  cstx,
  eadPubKeyCompressed,
  sourceShardMultiSigRedeemScript,
  destinationShardMultiSigRedeemScript,
) {
  // ToDo: [SECURITY] Validate 1 and 3 inputs also.
  let err = validateTxInputSignature(
    cstx,
    0,
    eadPubKeyCompressed,
    sourceShardMultiSigRedeemScript,
  );
  if (err != null) {
    return err;
  }
  err = validateTxInputSignature(
    cstx,
    2,
    eadPubKeyCompressed,
    destinationShardMultiSigRedeemScript,
  );
  if (err != null) {
    return err;
  }
  return null;
}
function validateTxInputSignature(
  cstx,
  inputIdx,
  pubKeyCompressed,
  multiSigRedeemScript,
) {
  const errInvalidSignature = Error(
    `signature of the input ${inputIdx} does not match pub key sent (${pubKeyCompressed.toString(
      'hex',
    )})`,
  );
  const node = index_1.ECPair.fromPublicKey(pubKeyCompressed, {
    compressed: true,
  });
  // console.log('node', node)
  if (inputIdx > cstx.ins.length - 1) {
    return Error('attempt to access non present input');
  }
  const input = cstx.ins[inputIdx];
  const tokens = index_1.script.decompile(input.script);
  for (const token of tokens) {
    if (typeof token === 'number') {
      continue;
    }
    try {
      const signature = index_1.script.signature.decode(token);
      const hashForSignature = cstx.hashForSignature(
        inputIdx,
        multiSigRedeemScript,
        signature.hashType,
      );
      // console.log('c:', c.toString('hex'));
      // console.log('signature:', signature.signature.toString('hex'));
      // console.log('hashForSignature:', hashForSignature.toString('hex'));
      const isSigned = node.verify(hashForSignature, signature.signature);
      if (!isSigned) {
        return errInvalidSignature;
      } else {
        return null;
      }
    } catch (e) {
      // ignore this token
    }
  }
  return errInvalidSignature;
}
