'use strict';
Object.defineProperty(exports, '__esModule', { value: true });
const networks_1 = require('../networks');
const bscript = require('../script');
const lazy = require('./lazy');
const OPS = bscript.OPS;
const typef = require('typeforce');
const ecc = require('tiny-secp256k1');
const OP_INT_BASE = OPS.OP_RESERVED; // OP_1 - 1
function stacksEqual(a, b) {
  if (a.length !== b.length) return false;
  return a.every((x, i) => {
    return x.equals(b[i]);
  });
}
// input: OP_0 [signatures ...]
// output:
// OP_INPUTAGE <required_age_for_refund>  OP_LESSTHAN
// OP_IF
//     <numsigs> <pubkey> <pubkey> <pubkey>... <numpubkeys>
//     OP_CHECKMULTISIG
// OP_ELSE
//     <refund_pubkey> OP_CHECKSIG OP_NIP
// OP_ENDIF
function p2mstl(inputOpts, opts) {
  if (
    !inputOpts.input &&
    !inputOpts.output &&
    !inputOpts.refundDeferringPeriod &&
    !inputOpts.refund &&
    !(inputOpts.pubkeys && inputOpts.m !== undefined) &&
    !inputOpts.signatures
  ) {
    throw new TypeError('Not enough data');
  }
  opts = Object.assign({ validate: true }, opts || {});
  function isAcceptableSignature(x) {
    return (
      bscript.isCanonicalScriptSignature(x) ||
      (opts.allowIncomplete && x === OPS.OP_0) !== undefined
    );
  }
  typef(
    {
      network: typef.maybe(typef.Object),
      sequenceLock: typef.maybe(typef.Number),
      refund: typef.maybe(typef.String),
      m: typef.maybe(typef.Number),
      n: typef.maybe(typef.Number),
      output: typef.maybe(typef.Buffer),
      pubkeys: typef.maybe(typef.arrayOf(ecc.isPoint)),
      signatures: typef.maybe(typef.arrayOf(isAcceptableSignature)),
      input: typef.maybe(typef.Buffer),
    },
    inputOpts,
  );
  const network = inputOpts.network || networks_1.bitcoin;
  const payment = { network };
  let chunks = [];
  let decoded = false;
  function decode(output) {
    if (decoded) return;
    decoded = true;
    chunks = bscript.decompile(output);
    if (typeof chunks[1] === 'number') {
      payment.refundDeferringPeriod = chunks[1] - OP_INT_BASE;
    } else {
      const rdp = chunks[1];
      switch (rdp.length) {
        case 1: {
          payment.refundDeferringPeriod = rdp.readInt8(0);
          break;
        }
        case 2: {
          payment.refundDeferringPeriod = rdp.readInt16LE(0);
          break;
        }
        case 4: {
          payment.refundDeferringPeriod = rdp.readInt32LE(0);
        }
      }
    }
    payment.m = chunks[4] - OP_INT_BASE;
    payment.n = chunks[chunks.length - 7] - OP_INT_BASE;
    payment.pubkeys = chunks.slice(5, -7);
    const refund = chunks[chunks.length - 4];
    payment.refund = refund.toString('hex');
    const haveRefund = payment.pubkeys.some(value => {
      return value.toString('hex') === payment.refund;
    });
    if (!haveRefund) {
      payment.pubkeys.push(refund);
    }
  }
  lazy.prop(payment, 'output', () => {
    if (!inputOpts.m) return;
    if (!payment.n) return;
    if (!inputOpts.refundDeferringPeriod) return;
    if (!inputOpts.pubkeys) return;
    if (!inputOpts.refund) return;
    const refundAddress = Buffer.from(inputOpts.refund, 'hex');
    const refundDeferringPeriod = inputOpts.refundDeferringPeriod;
    if (inputOpts.sortKeys) {
      inputOpts.pubkeys = inputOpts.pubkeys.sort(Buffer.compare);
    }
    return bscript.compile(
      [].concat(
        OPS.OP_INPUTAGE,
        bscript.encodeSmallNumber(refundDeferringPeriod),
        OPS.OP_LESSTHAN,
        OPS.OP_IF,
        OP_INT_BASE + inputOpts.m,
        inputOpts.pubkeys,
        OP_INT_BASE + payment.n,
        OPS.OP_CHECKMULTISIG,
        OPS.OP_ELSE,
        refundAddress,
        OPS.OP_CHECKSIG,
        OPS.OP_NIP,
        OPS.OP_ENDIF,
      ),
    );
  });
  lazy.prop(payment, 'm', () => {
    if (!payment.output) return;
    decode(payment.output);
    return payment.m;
  });
  lazy.prop(payment, 'refundDeferringPeriod', () => {
    if (!payment.output) return;
    decode(payment.output);
    return payment.refundDeferringPeriod;
  });
  lazy.prop(payment, 'n', () => {
    if (!payment.pubkeys) return;
    return payment.pubkeys.length;
  });
  lazy.prop(payment, 'pubkeys', () => {
    if (!inputOpts.output) return;
    decode(inputOpts.output);
    return payment.pubkeys;
  });
  lazy.prop(payment, 'signatures', () => {
    if (!inputOpts.input) return;
    return bscript.decompile(inputOpts.input).slice(1);
  });
  lazy.prop(payment, 'refund', () => {
    if (!payment.refund) return;
    return payment.refund;
  });
  lazy.prop(payment, 'input', () => {
    if (!inputOpts.signatures) return;
    return bscript.compile([OPS.OP_0].concat(inputOpts.signatures));
  });
  lazy.prop(payment, 'witness', () => {
    if (!payment.input) return;
    return [];
  });
  lazy.prop(payment, 'name', () => {
    if (!payment.m || !payment.n || !payment.refundDeferringPeriod) return;
    return `p2mstl(${payment.m} of ${payment.n})`;
  });
  // extended validation
  if (opts.validate) {
    if (inputOpts.output) {
      decode(inputOpts.output);
      if (chunks[0] !== OPS.OP_INPUTAGE)
        throw new TypeError('Output is invalid (OP_INPUTAGE not found)');
      let refundDeferringPeriod = 0;
      if (typeof chunks[1] === 'number') {
        refundDeferringPeriod = chunks[1] - OP_INT_BASE;
      } else {
        const rdp = chunks[1];
        switch (rdp.length) {
          case 1: {
            refundDeferringPeriod = rdp.readInt8(0);
            break;
          }
          case 2: {
            refundDeferringPeriod = rdp.readInt16LE(0);
            break;
          }
          case 4: {
            refundDeferringPeriod = rdp.readInt32LE(0);
          }
        }
      }
      if (refundDeferringPeriod <= 0) {
        throw new TypeError(
          'Output is invalid (required_age_for_refund is invalid - ' +
            refundDeferringPeriod +
            ')',
        );
      }
      if (chunks[2] !== OPS.OP_LESSTHAN)
        throw new TypeError('Output is invalid (OP_LESSTHAN not found)');
      if (chunks[3] !== OPS.OP_IF)
        throw new TypeError('Output is invalid (OP_IF not found)');
      if (!typef.Number(chunks[4]))
        throw new TypeError('Output is invalid (m is not a number)');
      if (!typef.Number(chunks[chunks.length - 7]))
        throw new TypeError('Output is invalid (n is not a number)');
      if (chunks[chunks.length - 6] !== OPS.OP_CHECKMULTISIG)
        throw new TypeError('Output is invalid (OP_CHECKMULTISIG not found)');
      if (chunks[chunks.length - 3] !== OPS.OP_CHECKSIG)
        throw new TypeError('Output is invalid (OP_CHECKSIG not found)');
      if (chunks[chunks.length - 2] !== OPS.OP_NIP)
        throw new TypeError('Output is invalid (OP_NIP not found)');
      if (chunks[chunks.length - 1] !== OPS.OP_ENDIF)
        throw new TypeError('Output is invalid (OP_ENDIF not found)');
      if (
        payment.m <= 0 ||
        payment.n > 16 ||
        payment.m > payment.n ||
        payment.n !== chunks.length - 12
      )
        throw new TypeError('Output is invalid (m and n values)');
      if (!payment.pubkeys.every(x => ecc.isPoint(x)))
        throw new TypeError('Output is invalid (not ecc.isPoint)');
      if (inputOpts.m !== undefined && inputOpts.m !== payment.m)
        throw new TypeError('m mismatch');
      if (
        inputOpts.refundDeferringPeriod !== undefined &&
        inputOpts.refundDeferringPeriod !== payment.refundDeferringPeriod
      )
        throw new TypeError('refundDeferringPeriod mismatch');
      if (inputOpts.n !== undefined && inputOpts.n !== payment.n)
        throw new TypeError('n mismatch');
      if (inputOpts.pubkeys && !stacksEqual(inputOpts.pubkeys, payment.pubkeys))
        throw new TypeError('Pubkeys mismatch');
    }
    if (inputOpts.pubkeys) {
      if (inputOpts.n !== undefined && inputOpts.n !== inputOpts.pubkeys.length)
        throw new TypeError('Pubkey count mismatch');
      payment.n = inputOpts.pubkeys.length;
      if (payment.n < payment.m)
        throw new TypeError('Pubkey count cannot be less than m');
    }
    if (inputOpts.signatures) {
      if (inputOpts.signatures.length < 1)
        throw new TypeError('Not enough signatures provided');
      if (inputOpts.signatures.length > payment.m)
        throw new TypeError('Too many signatures provided');
    }
    if (inputOpts.input) {
      if (inputOpts.input[0] !== OPS.OP_0)
        throw new TypeError('Input is invalid');
      if (
        payment.signatures.length === 0 ||
        !payment.signatures.every(isAcceptableSignature)
      )
        throw new TypeError('Input has invalid signature(s)');
      if (
        inputOpts.signatures &&
        !stacksEqual(inputOpts.signatures, payment.signatures)
      )
        throw new TypeError('Signature mismatch');
      if (
        inputOpts.m !== undefined &&
        inputOpts.m !== inputOpts.signatures.length
      )
        throw new TypeError('Signature count mismatch');
    }
  }
  return Object.assign(payment, inputOpts);
}
exports.p2mstl = p2mstl;
