'use strict';
Object.defineProperty(exports, '__esModule', { value: true });
const classify_1 = require('../classify');
const bcrypto = require('../crypto');
const networks_1 = require('../networks');
const bscript = require('../script');
const htlcTmpl = require('../templates/htlc');
const lazy = require('./lazy');
const typef = require('typeforce');
const OPS = bscript.OPS;
const ecc = require('tiny-secp256k1');
const bs58check = require('bs58check');
// input: {signature}
// output: {pubKey} OP_CHECKSIG
//
// input: {signature} {pubkey}
// output: OP_DUP OP_HASH160 {hash160(pubkey)} OP_EQUALVERIFY OP_CHECKSIG
//
// input: [redeemScriptSig ...] {redeemScript}
// output: OP_HASH160 {hash160(redeemScript)} OP_EQUAL
function p2htlc(inPayment, opts) {
  if (
    !inPayment.input &&
    !inPayment.output &&
    !inPayment.pubkey &&
    !inPayment.input &&
    !inPayment.signature &&
    !inPayment.hash &&
    !inPayment.redeem
  )
    throw new TypeError('Not enough data');
  opts = Object.assign({ validate: true }, opts || {});
  typef(
    {
      network: typef.maybe(typef.Object),
      lockPeriod: typef.maybe(typef.Number),
      address: typef.maybe(typef.String),
      hash: typef.maybe(typef.Buffer),
      output: typef.maybe(typef.Buffer),
      pubkey: typef.maybe(ecc.isPoint),
      signature: typef.maybe(bscript.isCanonicalScriptSignature),
      input: typef.maybe(typef.Buffer),
      redeem: typef.maybe({
        network: typef.maybe(typef.Object),
        output: typef.maybe(typef.Buffer),
        input: typef.maybe(typef.Buffer),
        witness: typef.maybe(typef.arrayOf(typef.Buffer)),
      }),
    },
    inPayment,
  );
  const network = inPayment.network || networks_1.bitcoin;
  const outPayment = { name: 'p2htlc', network };
  const _chunks = lazy.value(() => {
    return bscript.decompile(inPayment.input);
  });
  const _address = lazy.value(() => {
    const payload = bs58check.decode(inPayment.address);
    const version = payload.readUInt8(0);
    const hash = payload.slice(1);
    return { version, hash };
  });
  lazy.prop(outPayment, 'address', () => {
    if (!outPayment.hash) return;
    if (!outPayment.output) return;
    const out = outPayment.output;
    const payload = Buffer.allocUnsafe(out.length + 1);
    payload.writeUInt8(network.htlc, 0);
    out.copy(payload, 1);
    return bs58check.encode(payload);
  });
  lazy.prop(outPayment, 'hash', () => {
    if (inPayment.subType === classify_1.types.P2SH) {
      if (inPayment.redeem && inPayment.redeem.output)
        return bcrypto.hash160(inPayment.redeem.output);
    }
    if (inPayment.output) return inPayment.output.slice(3, 23);
    if (inPayment.address) return _address().hash;
    if (inPayment.pubkey || outPayment.pubkey)
      return bcrypto.hash160(inPayment.pubkey || outPayment.pubkey);
  });
  lazy.prop(outPayment, 'output', () => {
    if (!inPayment.lockPeriod) return;
    if (
      inPayment.subType === classify_1.types.P2SH &&
      (inPayment.hash || inPayment.redeem)
    ) {
      if (!outPayment.hash) return;
      return bscript.compile([
        OPS.OP_INPUTAGE,
        bscript.encodeSmallNumber(inPayment.lockPeriod),
        OPS.OP_GREATERTHAN,
        OPS.OP_IF,
        OPS.OP_HASH160,
        outPayment.hash,
        OPS.OP_EQUAL,
        OPS.OP_ELSE,
        OPS.OP_RETURN,
        OPS.OP_ENDIF,
      ]);
    }
    if (!inPayment.pubkey) return;
    if (inPayment.pubkey) {
      if (inPayment.subType === classify_1.types.P2PK) {
        return bscript.compile([
          OPS.OP_INPUTAGE,
          bscript.encodeSmallNumber(inPayment.lockPeriod),
          OPS.OP_GREATERTHAN,
          OPS.OP_IF,
          inPayment.pubkey,
          OPS.OP_CHECKSIG,
          OPS.OP_ELSE,
          OPS.OP_RETURN,
          OPS.OP_ENDIF,
        ]);
      }
      if (!outPayment.hash) return;
      return bscript.compile([
        OPS.OP_INPUTAGE,
        bscript.encodeSmallNumber(inPayment.lockPeriod),
        OPS.OP_GREATERTHAN,
        OPS.OP_IF,
        OPS.OP_DUP,
        OPS.OP_HASH160,
        outPayment.hash,
        OPS.OP_EQUALVERIFY,
        OPS.OP_CHECKSIG,
        OPS.OP_ELSE,
        OPS.OP_RETURN,
        OPS.OP_ENDIF,
      ]);
    }
  });
  // lazy.prop(outPayment, 'pubkey', () => {
  //   if (!inPayment.output) return;
  //   return inPayment.output.slice(1, -1);
  // });
  lazy.prop(outPayment, 'signature', () => {
    if (!inPayment.input) return;
    return _chunks()[0];
  });
  lazy.prop(outPayment, 'subType', () => {
    if (inPayment.input) {
      const c = _chunks();
      if (c.length === 1) return classify_1.types.P2PK;
      if (c.length > 2) return classify_1.types.P2SH;
      if (c.length === 2 && bscript.isCanonicalPubKey(c[1]))
        return classify_1.types.P2PKH;
    }
    if (inPayment.output) {
      return htlcTmpl.output.getSubType(inPayment.output);
    }
    return;
  });
  lazy.prop(outPayment, 'input', () => {
    switch (outPayment.subType) {
      case classify_1.types.P2PK: {
        if (!inPayment.signature) return;
        return bscript.compile([inPayment.signature]);
      }
      case classify_1.types.P2PKH: {
        if (!inPayment.pubkey) return;
        if (!inPayment.signature) return;
        return bscript.compile([inPayment.signature, inPayment.pubkey]);
      }
      case classify_1.types.P2SH: {
        if (
          !inPayment.redeem ||
          !inPayment.redeem.input ||
          !inPayment.redeem.output
        )
          return;
        return bscript.compile(
          [].concat(
            bscript.decompile(inPayment.redeem.input),
            inPayment.redeem.output,
          ),
        );
      }
    }
  });
  // extended validation
  if (opts.validate) {
    if (inPayment.output) {
      // output: {pubKey} OP_CHECKSIG
      // output: OP_DUP OP_HASH160 {hash160(pubkey)} OP_EQUALVERIFY OP_CHECKSIG
      // output: OP_HASH160 {hash160(redeemScript)} OP_EQUAL
      if (!htlcTmpl.output.check(inPayment.output))
        throw new TypeError('Output is invalid');
    }
    if (inPayment.signature) {
      if (inPayment.input && !inPayment.input.equals(outPayment.input))
        throw new TypeError('Signature mismatch');
    }
    if (inPayment.input) {
      if (_chunks().length !== 1) throw new TypeError('Input is invalid');
      if (!bscript.isCanonicalScriptSignature(outPayment.signature))
        throw new TypeError('Input has invalid signature');
    }
  }
  return Object.assign(outPayment, inPayment);
}
exports.p2htlc = p2htlc;
