'use strict';
Object.defineProperty(exports, '__esModule', { value: true });
const bscript = require('../../script');
const script_1 = require('../../script');
const types = require('../../types');
const OP_INT_BASE = script_1.OPS.OP_RESERVED; // OP_1 - 1
// The minimal valid MultiSigLockTy:
// [ 0] OP_INPUTAGE
// [ 1] <required_age_for_refund>
// [ 2] OP_LESSTHAN
// [ 3] OP_IF
// [ 4]    OP_1
// [..]    <pubkey>
// [-7]    OP_1
// [-6]    OP_CHECKMULTISIG
// [-5] OP_ELSE
// [-4]    <refund_pubkey>
// [-3]    OP_CHECKSIG
// [-2]    OP_NIP
// [-1] OP_ENDIF
function check(script, allowIncomplete) {
  const chunks = bscript.decompile(script);
  if (chunks.length < 13) return false;
  if (chunks[0] !== script_1.OPS.OP_INPUTAGE) return false;
  if (!types.Number(chunks[1])) return false;
  if (chunks[2] !== script_1.OPS.OP_LESSTHAN) return false;
  if (chunks[3] !== script_1.OPS.OP_IF) return false;
  if (chunks[chunks.length - 5] !== script_1.OPS.OP_ELSE) return false;
  const refund = chunks[chunks.length - 4];
  if (!bscript.isCanonicalPubKey(refund)) return false;
  if (chunks[chunks.length - 3] !== script_1.OPS.OP_CHECKSIG) return false;
  if (chunks[chunks.length - 2] !== script_1.OPS.OP_NIP) return false;
  if (chunks[chunks.length - 1] !== script_1.OPS.OP_ENDIF) return false;
  const multisig = chunks.slice(4, -7);
  if (multisig[multisig.length - 1] !== script_1.OPS.OP_CHECKMULTISIG)
    return false;
  if (!types.Number(multisig[0])) return false;
  if (!types.Number(multisig[multisig.length - 2])) return false;
  const m = chunks[0] - OP_INT_BASE;
  const n = chunks[chunks.length - 2] - OP_INT_BASE;
  if (m <= 0) return false;
  if (n > 16) return false;
  if (m > n) return false;
  if (n !== multisig.length - 3) return false;
  if (allowIncomplete) return true;
  const keys = multisig.slice(1, -2);
  return keys.every(bscript.isCanonicalPubKey);
}
exports.check = check;
check.toJSON = () => {
  return 'multisig_lock output';
};
