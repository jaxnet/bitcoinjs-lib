'use strict';
// {signature}
// {signature} {pubKey}
// <scriptSig> {serialized scriptPubKey script}
Object.defineProperty(exports, '__esModule', { value: true });
const bscript = require('../../script');
const p2sh = require('../scripthash/input');
function check(script, allowIncomplete) {
  const chunks = bscript.decompile(script);
  if (chunks.length < 1) return false;
  const isSignaturePresent = bscript.isCanonicalScriptSignature(chunks[0]);
  if (chunks.length === 1) return isSignaturePresent;
  const isPubKey = bscript.isCanonicalPubKey(chunks[1]);
  if (isPubKey) return true;
  return p2sh.check(script, allowIncomplete);
}
exports.check = check;
check.toJSON = () => {
  return 'htlc input';
};
