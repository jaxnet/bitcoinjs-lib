'use strict';
Object.defineProperty(exports, '__esModule', { value: true });
const bscript = require('../../script');
const script_1 = require('../../script');
const snum = require('../../script_number');
const types = require('../../types');
// HTLCScript is a lock script is of the form:
// [ 0] OP_INPUTAGE
// [ 1] <lockPeriod>
// [ 2] OP_GREATERTHAN
// [ 3] OP_IF
// [-or-] [4]OP_DUP [5]OP_HASH160  [6]<AddressPubKeyHash> [7]OP_EQUALVERIFY [8]OP_CHECKSIG
// [-or-] [4]<AddressPubKey> [5]OP_CHECKSIG
// [-or-] [4]OP_HASH160 [5]<AddressScriptHash> [6]OP_EQUAL
// [-3] OP_ELSE
// [-2]     OR_RETURN
// [-1] OP_ENDIF
function check(script) {
  const chunks = bscript.decompile(script);
  if (chunks.length !== 9 && chunks.length !== 10 && chunks.length !== 12)
    return false;
  if (chunks[0] !== script_1.OPS.OP_INPUTAGE) return false;
  if (!types.Number(chunks[1]) && !types.Number(snum.decode(chunks[1])))
    return false;
  if (chunks[2] !== script_1.OPS.OP_GREATERTHAN) return false;
  if (chunks[3] !== script_1.OPS.OP_IF) return false;
  if (chunks[chunks.length - 3] !== script_1.OPS.OP_ELSE) return false;
  if (chunks[chunks.length - 2] !== script_1.OPS.OP_RETURN) return false;
  if (chunks[chunks.length - 1] !== script_1.OPS.OP_ENDIF) return false;
  if (chunks.length === 9) {
    return (
      bscript.isCanonicalPubKey(chunks[4]) &&
      chunks[5] === script_1.OPS.OP_CHECKSIG
    );
  }
  if (chunks.length === 10) {
    if (chunks[4] !== script_1.OPS.OP_HASH160) return false;
    if (chunks[5].length !== 20) return false;
    if (chunks[6] !== script_1.OPS.OP_EQUAL) return false;
  }
  if (chunks.length === 12) {
    if (chunks[4] !== script_1.OPS.OP_DUP) return false;
    if (chunks[5] !== script_1.OPS.OP_HASH160) return false;
    if (chunks[6].length !== 20) return false;
    if (chunks[7] !== script_1.OPS.OP_EQUALVERIFY) return false;
    if (chunks[8] !== script_1.OPS.OP_CHECKSIG) return false;
  }
  return true;
}
exports.check = check;
function getSubType(script) {
  const chunks = bscript.decompile(script);
  if (chunks.length !== 9 && chunks.length !== 10 && chunks.length !== 12)
    return 'nonstandard';
  if (chunks.length === 9) {
    return 'pubkey';
  }
  if (chunks.length === 10) {
    return 'scripthash';
  }
  if (chunks.length === 12) {
    return 'pubkeyhash';
  }
  return 'nonstandard';
}
exports.getSubType = getSubType;
check.toJSON = () => {
  return 'multisig_lock output';
};
