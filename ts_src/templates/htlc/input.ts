// {signature}
// {signature} {pubKey}
// <scriptSig> {serialized scriptPubKey script}

import {Stack} from '../../payments';
import * as bscript from '../../script';
import * as p2sh from '../scripthash/input';

export function check(
  script: Buffer | Stack,
  allowIncomplete?: boolean,
): boolean {
  const chunks = bscript.decompile(script) as Stack;
  if (chunks.length < 1) return false;

  const isSignaturePresent = bscript.isCanonicalScriptSignature(chunks[0] as Buffer);
  if (chunks.length === 1) return isSignaturePresent;

  const isPubKey = bscript.isCanonicalPubKey(chunks[1] as Buffer);
  if (isPubKey) return true;

  return p2sh.check(script, allowIncomplete);
}

check.toJSON = (): string => {
  return 'htlc input';
};
