import {Stack} from '../../payments';
import * as bscript from '../../script';
import {OPS} from '../../script';
import * as snum from '../../script_number';
import * as types from '../../types';

// HTLCScript is a lock script is of the form:
// [ 0] OP_INPUTAGE
// [ 1] <lockPeriod>
// [ 2] OP_GREATERTHAN
// [ 3] OP_IF
// [-or-] [4]OP_DUP [5]OP_HASH160  [6]<AddressPubKeyHash> [7]OP_EQUALVERIFY [8]OP_CHECKSIG
// [-or-] [4]<AddressPubKey> [5]OP_CHECKSIG
// [-or-] [4]OP_HASH160 [5]<AddressScriptHash> [6]OP_EQUAL
// [-3] OP_ELSE
// [-2]     OR_RETURN
// [-1] OP_ENDIF
export function check(script: Buffer | Stack): boolean {
  const chunks = bscript.decompile(script) as Stack;
  if (chunks.length !== 9 && chunks.length !== 10 && chunks.length !== 12) return false;

  if (chunks[0] !== OPS.OP_INPUTAGE) return false;

  if (
    !types.Number(chunks[1]) &&
    !types.Number(snum.decode(chunks[1] as Buffer))
  ) return false;

  if (chunks[2] !== OPS.OP_GREATERTHAN) return false;
  if (chunks[3] !== OPS.OP_IF) return false;
  if (chunks[chunks.length - 3] !== OPS.OP_ELSE) return false;
  if (chunks[chunks.length - 2] !== OPS.OP_RETURN) return false;
  if (chunks[chunks.length - 1] !== OPS.OP_ENDIF) return false;

  if (chunks.length === 9) {
    return bscript.isCanonicalPubKey(chunks[4] as Buffer) &&
      chunks[5] === OPS.OP_CHECKSIG;
  }

  if (chunks.length === 10) {
    if (chunks[4] !== OPS.OP_HASH160) return false;
    if ((chunks[5] as Buffer).length !== 20) return false;
    if (chunks[6] !== OPS.OP_EQUAL) return false;

  }

  if (chunks.length === 12) {
    if (chunks[4] !== OPS.OP_DUP) return false;
    if (chunks[5] !== OPS.OP_HASH160) return false;
    if ((chunks[6] as Buffer).length !== 20) return false;
    if (chunks[7] !== OPS.OP_EQUALVERIFY) return false;
    if (chunks[8] !== OPS.OP_CHECKSIG) return false;
  }
  return true;
}

export function getSubType(script: Buffer | Stack): string {
  const chunks = bscript.decompile(script) as Stack;
  if (chunks.length !== 9 && chunks.length !== 10 && chunks.length !== 12) return 'nonstandard';

  if (chunks.length === 9) {
    return 'pubkey';
  }

  if (chunks.length === 10) {
    return 'scripthash';
  }

  if (chunks.length === 12) {
    return 'pubkeyhash';
  }

  return 'nonstandard';
}

check.toJSON = (): string => {
  return 'multisig_lock output';
};
