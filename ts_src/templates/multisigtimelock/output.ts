import { Stack } from '../../payments';
import * as bscript from '../../script';
import { OPS } from '../../script';
import * as types from '../../types';

const OP_INT_BASE = OPS.OP_RESERVED; // OP_1 - 1

// The minimal valid MultiSigLockTy:
// [ 0] OP_INPUTAGE
// [ 1] <required_age_for_refund>
// [ 2] OP_LESSTHAN
// [ 3] OP_IF
// [ 4]    OP_1
// [..]    <pubkey>
// [-7]    OP_1
// [-6]    OP_CHECKMULTISIG
// [-5] OP_ELSE
// [-4]    <refund_pubkey>
// [-3]    OP_CHECKSIG
// [-2]    OP_NIP
// [-1] OP_ENDIF
export function check(
  script: Buffer | Stack,
  allowIncomplete?: boolean,
): boolean {
  const chunks = bscript.decompile(script) as Stack;
  if (chunks.length < 13) return false;

  if (chunks[0] !== OPS.OP_INPUTAGE) return false;
  if (!types.Number(chunks[1])) return false;
  if (chunks[2] !== OPS.OP_LESSTHAN) return false;
  if (chunks[3] !== OPS.OP_IF) return false;
  if (chunks[chunks.length - 5] !== OPS.OP_ELSE) return false;

  const refund = chunks[chunks.length - 4] as Buffer;

  if (!bscript.isCanonicalPubKey(refund)) return false;
  if (chunks[chunks.length - 3] !== OPS.OP_CHECKSIG) return false;
  if (chunks[chunks.length - 2] !== OPS.OP_NIP) return false;
  if (chunks[chunks.length - 1] !== OPS.OP_ENDIF) return false;

  const multisig = chunks.slice(4, -7);

  if (multisig[multisig.length - 1] !== OPS.OP_CHECKMULTISIG) return false;
  if (!types.Number(multisig[0])) return false;
  if (!types.Number(multisig[multisig.length - 2])) return false;

  const m = (chunks[0] as number) - OP_INT_BASE;
  const n = (chunks[chunks.length - 2] as number) - OP_INT_BASE;
  if (m <= 0) return false;
  if (n > 16) return false;
  if (m > n) return false;
  if (n !== multisig.length - 3) return false;
  if (allowIncomplete) return true;
  const keys = multisig.slice(1, -2) as Buffer[];
  return keys.every(bscript.isCanonicalPubKey);
}

check.toJSON = (): string => {
  return 'multisig_lock output';
};
