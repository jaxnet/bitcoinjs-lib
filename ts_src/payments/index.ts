import {Network} from '../networks';
import {p2data as embed} from './embed';
import {p2htlc} from './p2htlc';
import {p2ms} from './p2ms';
import {p2mstl} from './p2mstl';
import {p2pk} from './p2pk';
import {p2pkh} from './p2pkh';
import {p2sh} from './p2sh';
import {p2wpkh} from './p2wpkh';
import {p2wsh} from './p2wsh';

export interface Payment {
  name?: string;
  network?: Network;
  output?: Buffer;
  data?: Buffer[];
  refundDeferringPeriod?: number;
  refund?: string;
  m?: number;
  n?: number;
  pubkeys?: Buffer[];
  input?: Buffer;
  signatures?: Buffer[];
  pubkey?: Buffer;
  signature?: Buffer;
  address?: string;
  hash?: Buffer;
  redeem?: Payment;
  witness?: Buffer[];
  lockPeriod?: number;
  subType?: string;
  sortKeys?: boolean;
}

export type PaymentCreator = (a: Payment, opts?: PaymentOpts) => Payment;

export type PaymentFunction = () => Payment;

export interface PaymentOpts {
  validate?: boolean;
  allowIncomplete?: boolean;
}

export type StackElement = Buffer | number;
export type Stack = StackElement[];
export type StackFunction = () => Stack;

export {embed, p2ms, p2mstl, p2pk, p2pkh, p2sh, p2wpkh, p2wsh, p2htlc};

// TODO
// witness commitment
