import base58 = require('bs58');
import {types} from './classify';
import * as bitcoin from './index';

// tslint:disable-next-line:typedef
function decodePrivateKey(strKey: string) {
  return bitcoin.ECPair.fromPrivateKey(
    Buffer.from(strKey, 'hex'), {compressed: true});
}

const testDataSet = {
  privateKey: decodePrivateKey(''),
  addressPubKey: '037d438821f92e638f1b1d31cfa466b7b7d12dbc6151e9428690396014bd84ccd9',
  htlcPubKey: '55EBchh4zTcccRNgLGCfPCV7N3Bkxb2L3H6NbasyBAE8MRY4QwEioNHGG6ZakZv8jibM',
  addressHash: '1ENnTs1g4RhzjgJXbkiWeZh7Bvi4cN3MpG',
  htlcPubKeyHash: 'HRdJ8Yet7G3vWBdwwdacXv7qGg5ei456we1QQNGVt7zHPhZuQh3dBe',
  lockPeriod: 42357,
};

console.log(testDataSet.privateKey.publicKey.toString('hex'));
console.log(testDataSet.privateKey.publicKey.toString('hex') === testDataSet.addressPubKey);

const htlcPubKey = bitcoin.payments.p2htlc({
  lockPeriod: testDataSet.lockPeriod,
  pubkey: testDataSet.privateKey.publicKey,
  subType: types.P2PK,
});

console.log('testDataSet.pubkey: ', testDataSet.privateKey.publicKey);
console.log('htlcPubKey.pubkey:  ', htlcPubKey.pubkey);
console.log('');
console.log('htlcPubKey.output:  ', base58.decode(htlcPubKey.address!));
console.log('testDataSet.output: ', base58.decode(testDataSet.htlcPubKey));
console.log('');
console.log('htlcPubKey.address:     ', htlcPubKey.address);
console.log('testDataSet.htlcPubKey: ', testDataSet.htlcPubKey);
console.log('');

const htlcPubKeyHash = bitcoin.payments.p2htlc({
  lockPeriod: testDataSet.lockPeriod,
  pubkey: testDataSet.privateKey.publicKey,
  subType: types.P2PKH,
});

console.log('htlcPubKeyHash.output: ', base58.decode(htlcPubKeyHash.address!));
console.log('testDataSet.output:    ', base58.decode(testDataSet.htlcPubKeyHash));
console.log('');
console.log('htlcPubKeyHash.address:     ', htlcPubKeyHash.address);
console.log('testDataSet.htlcPubKeyHash: ', testDataSet.htlcPubKeyHash);

console.log('');

const testDataMultisig = {
  redeemScript: '5221037d438821f92e638f1b1d31cfa466b7b7d12dbc6151e9428690396014bd84ccd' +
    '92103b0a5c96640185fca39a5dc5d3602ece1526ded1aa1807e5ecf18452d7dcfea0c52ae',
  htlcScriptHash: 'qu8dkjxSMtDK39Yo69pBxWpx4CxWxEWM8U55i2EFS4baaffJMK4',
};

const multisigScript = bitcoin.payments.p2ms({output: Buffer.from(testDataMultisig.redeemScript, 'hex')});

const scriptHash = bitcoin.payments.p2sh({redeem: multisigScript});

const htlcScriptHashFromHash = bitcoin.payments.p2htlc({
  lockPeriod: testDataSet.lockPeriod,
  hash: scriptHash.hash,
  subType: types.P2SH,
});

const htlcScriptHashFromRedeem = bitcoin.payments.p2htlc({
  lockPeriod: testDataSet.lockPeriod,
  redeem: {output: Buffer.from(testDataMultisig.redeemScript, 'hex')},
  subType: types.P2SH,
});

console.log('scriptHash:    ', scriptHash.hash);
console.log('scriptHash:    ', scriptHash.output);
console.log('htlcScriptHash:', base58.decode(testDataMultisig.htlcScriptHash));
console.log('htlcFromHash:  ', htlcScriptHashFromHash.output);
console.log('htlcFromRedeem:', htlcScriptHashFromRedeem.output);
console.log('');
console.log('htlcScriptHash:', testDataMultisig.htlcScriptHash);
console.log('htlcFromHash:  ', htlcScriptHashFromHash.address);
console.log('htlcFromRedeem:', htlcScriptHashFromRedeem.address);

// parent tx:
// in: coinbase
// outs:
//   - alice_pubkey
//   - alice_pubkey_hash
//   - bob_pubkey
//   - bob_pubkey_hash
//   - alice_bob_script_hash

// new tx:
// in: alice_pubkey
// in: alice_pubkey_hash
console.log('');
console.log('');

const coinbaseTx = new bitcoin.Transaction();

const zeroArr: number[] = [];
for (let i = 0; i < 32; i++) zeroArr.push(0);

coinbaseTx.ins.push({
  index: 0,
  script: Buffer.from(testDataSet.addressPubKey, 'hex'),
  sequence: 4294967295,
  witness: [],
  hash: Buffer.from(zeroArr),
});
coinbaseTx.addOutput(htlcPubKey.output!, 12_003_000);
coinbaseTx.addOutput(htlcPubKeyHash.output!, 32_003_000);

try {
  bitcoin.payments.p2htlc({output: htlcPubKeyHash.output!});
  console.log('ok');
} catch (err) {
  console.log('err', err);

}
const coinbaseHex = coinbaseTx.toHex();

let psbt = new bitcoin.Psbt({network: bitcoin.networks.bitcoin});
psbt.addInput({
  hash: coinbaseTx.getHash(),
  index: 0,
  // redeemScript: Buffer.from(data.sourceRedeem, 'hex'),
  nonWitnessUtxo: Buffer.from(coinbaseHex, 'hex'),
});

psbt.addOutput({
  value: 11_903_000,
  address: '1ENnTs1g4RhzjgJXbkiWeZh7Bvi4cN3MpG',
});

psbt.signAllInputs(testDataSet.privateKey);
psbt.validateSignaturesOfAllInputs();
psbt.finalizeAllInputs();
console.log('signed tx:', psbt.extractTransaction(true).toHex());

psbt = new bitcoin.Psbt({network: bitcoin.networks.bitcoin});
psbt.addInput({
  hash: coinbaseTx.getHash(),
  index: 1,
  nonWitnessUtxo: Buffer.from(coinbaseHex, 'hex'),
});

psbt.addOutput({
  value: 11_903_000,
  address: '1ENnTs1g4RhzjgJXbkiWeZh7Bvi4cN3MpG',
});

psbt.signAllInputs(testDataSet.privateKey);
psbt.validateSignaturesOfAllInputs();
psbt.finalizeAllInputs();
console.log('signed tx:', psbt.extractTransaction(true).toHex());
