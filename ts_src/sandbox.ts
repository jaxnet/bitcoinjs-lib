import * as bitcoin from './index';

const data = {
  cstx: '0100010004dd4a00c6b2bb3f718657e568009c0c858f5c06a82e0a3982944a139203079637010000004900473044022038914eef9821532c9c11542659a668e807e77af7ef39682c058023c8f343570a02204f605cff0a255a9252225e00fce413ba7416cafc6c9b074063034270782aa9fc01ffffffff9b91f5f9c555d77e891794f906e58b7fbc52977448243fe78ae918ff0cf1f293010000008b483045022100973793a1543eee82aff50851b9860c9eaf7ed60712e966faae76046be223dea00220541875a2c1041f31a7d57c996536dac9131feeddaf5e53cef38babffa9d1ce770141043fc5c0b936bdb1595ca4807ebab7dfc5418abb6c030e58ea6b4270e524d3da9c9ff6ad1798097b14571fe21b2325c55a685ab5846848a7e4de8dc34f3231c88fffffffff17844dc3240d5240c72aee539661c1c54a47259ac40ccfa343d6fb3218bc7997000000004a00483045022100cdf8c4db6763984fafefd951c6aa5fa4a5610b4774fbb7b08301137525c3360502206afeb8033493df8900bdd2361db5da569e67082c14c80aac3c5cecb006bb0f0a01ffffffff17844dc3240d5240c72aee539661c1c54a47259ac40ccfa343d6fb3218bc7997010000008b4830450221009164cdcaab6ee71290da78360da980ceb22bcedd0b99b59371802c5574c3280d02205c2d303dd5b28544be9c0fe8596057efdddff39d9528150ddee46be1359b23700141043fc5c0b936bdb1595ca4807ebab7dfc5418abb6c030e58ea6b4270e524d3da9c9ff6ad1798097b14571fe21b2325c55a685ab5846848a7e4de8dc34f3231c88fffffffff04f00a0000000000001976a9145496c151b3a251c77b5544b1d3ac6242587215e888aca0410100000000001976a9145496c151b3a251c77b5544b1d3ac6242587215e888acd40a0000000000001976a9149bd8681d7f972de68b1ce72e534da3d2f85782f288ace5050000000000001976a9145496c151b3a251c77b5544b1d3ac6242587215e888ac00000000',
  sourceRedeem: 'bc02f0009f635241043a465d4b86a2161421a52ca9bdfb9cafaac8c8dd284e361331cca7e8fd678925471c687ccb18cf04e2ab2c12a74e51c272e5aad2c1b2529ac0d061b949498e5841043fc5c0b936bdb1595ca4807ebab7dfc5418abb6c030e58ea6b4270e524d3da9c9ff6ad1798097b14571fe21b2325c55a685ab5846848a7e4de8dc34f3231c88f52ae6741043a465d4b86a2161421a52ca9bdfb9cafaac8c8dd284e361331cca7e8fd678925471c687ccb18cf04e2ab2c12a74e51c272e5aad2c1b2529ac0d061b949498e58ac7768',
  sourceShardAmountUTXO: '0200000001f1ef8fe831d0c5f0c0decd0dc210e68b5b0f20995ee54a614a9c2865f01ad26d000000006a473044022072fb3dea4693c28e713605386521b95546624eae5efab29fa866ad4b1129709702207295f57b41e0234c9c9a412df9cf550393056c8429eabd0b49a944b2ca4e18470121023a465d4b86a2161421a52ca9bdfb9cafaac8c8dd284e361331cca7e8fd678925ffffffff02cc590100000000001976a9149bd8681d7f972de68b1ce72e534da3d2f85782f288acf00a00000000000017a914400bed348d2966632bc84b28fa87232d99a5b8118700000000',
  // senderPKeyHex: '',
  senderPKeyHex: '',
};

const cstx = bitcoin.Transaction.fromHex(data.cstx);
console.log(cstx.ins);

const sender = bitcoin.ECPair.fromPrivateKey(Buffer.from(data.senderPKeyHex, 'hex'), {compressed: false});

let arg = {
  tx: cstx,
  redeemScript: Buffer.from(data.sourceRedeem, 'hex'),
  parentTx: Buffer.from(data.sourceShardAmountUTXO, 'hex'),
};

{
  const lockScript = bitcoin.payments.p2mstl({output: arg.redeemScript});
  const keys = lockScript.pubkeys!.map(el => el.toString('hex'));

  console.log('sender.publicKey', sender.publicKey.toString('hex'));
  console.log('keys', keys);
  console.log('keys', lockScript.refund);
}

console.log('sig before:', arg.tx.ins[0].script.toString('hex'));
arg = bitcoin.addSignatureToTx(arg, 0, sender);

console.log('sig after:', arg.tx.ins[0].script.toString('hex'));
console.log('tx after:', arg.tx.toHex());

// -----

const psbt = new bitcoin.Psbt({network: bitcoin.networks.testnet});

psbt.addInput({
  hash: cstx.ins[0].hash,
  index: cstx.ins[0].index,
  redeemScript: Buffer.from(data.sourceRedeem, 'hex'),
  nonWitnessUtxo: Buffer.from(data.sourceShardAmountUTXO, 'hex'),
});

psbt.addOutput({
  value: cstx.outs[0].value,
  address: 'mxQsksaTJb11i7vSxAUL6VBjoQnhP3bfFz',
});

psbt.signAllInputs(sender);
psbt.validateSignaturesOfAllInputs();
psbt.finalizeAllInputs();

const hex = psbt.extractTransaction().toHex();

console.log('refund hex', hex);
